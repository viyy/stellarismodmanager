﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Win32;
using StellarisModManager.Annotations;

namespace StellarisModManager
{
    /// <summary>
    ///     Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private ObservableCollection<Cmod> _activeMods = new ObservableCollection<Cmod>();

        private Cmod _activeModSelected;
        private ObservableCollection<Cmod> _allMods = new ObservableCollection<Cmod>();
        private Cmod _allModSelected;

        private string _status = "";

        public MainWindow()
        {
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                  "\\StellarisModManager\\"))
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                          "\\StellarisModManager\\");
            var allMods = Cmod.GetAvailableMods();
            var activeMods = Cmod.GetActiveMods();
            foreach (var mod in activeMods)
            {
                if (!Cmod.CheckMod(mod.Path))
                    mod.IsFound = false;
                mod.PathSimple = mod.Path.Replace("ugc_", "").Replace(".mod", "");
            }
            foreach (var mod in allMods)
            {
                if (!Cmod.CheckMod(mod.Path))
                    mod.IsFound = false;
                mod.PathSimple = mod.Path.Replace("ugc_", "").Replace(".mod", "");
            }
            ActiveMods = new ObservableCollection<Cmod>(activeMods);
            AllMods = new ObservableCollection<Cmod>(allMods);
            InitializeComponent();
        }

        public static string Version => Properties.Resources.Version;
        public static string Author => "By " + Properties.Resources.Author;

        public ObservableCollection<Cmod> ActiveMods
        {
            get => _activeMods;
            set
            {
                if (_activeMods == value) return;
                _activeMods = value;
                OnPropertyChanged(nameof(ActiveMods));
            }
        }

        public ObservableCollection<Cmod> AllMods
        {
            get => _allMods;
            set
            {
                if (_allMods == value) return;
                _allMods = value;
                OnPropertyChanged(nameof(AllMods));
            }
        }

        public int ActiveModsCount => ActiveMods.Count;

        public int AllModsCount => AllMods.Count;

        public Cmod ActiveModSelected
        {
            get => _activeModSelected;
            set
            {
                if (_activeModSelected == value) return;
                _activeModSelected = value;
                OnPropertyChanged(nameof(ActiveModSelected));
            }
        }

        public Cmod AllModSelected
        {
            get => _allModSelected;
            set
            {
                if (_allModSelected == value) return;
                _allModSelected = value;
                OnPropertyChanged(nameof(AllModSelected));
            }
        }

        public string Hash => Cmod.GetHashOfList(ActiveMods.ToList());

        public string Status
        {
            get => _status;
            set
            {
                if (_status == value) return;
                _status = value;
                OnPropertyChanged(nameof(Status));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new SaveFileDialog
            {
                FileName = "m" + ActiveModsCount + "_h" + Hash,
                DefaultExt = ".txt",
                Filter = "*.txt|*.txt|*.*|All files",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                   "\\StellarisModManager\\"
            };
            if (dlg.ShowDialog() == true)
                Cmod.SaveCollection(dlg.FileName, ActiveMods.ToList());
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenFileDialog
            {
                DefaultExt = ".txt",
                Filter = "*.txt|*.txt|*.*|All files",
                Multiselect = false,
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                   "\\StellarisModManager\\"
            };
            if (dlg.ShowDialog() != true) return;
            ActiveMods = new ObservableCollection<Cmod>(Cmod.LoadCollection(dlg.FileName));
            foreach (var mod in ActiveMods)
            {
                if (!Cmod.CheckMod(mod.Path))
                    mod.IsFound = false;
                mod.PathSimple = mod.Path.Replace("ugc_", "").Replace(".mod", "");
            }
            ActiveModSelected = null;
            OnPropertyChanged(nameof(ActiveModSelected));
            OnPropertyChanged(nameof(Hash));
            Status = "";
        }

        private void BtnDel_Click(object sender, RoutedEventArgs e)
        {
            if (ActiveModSelected == null) return;
            ActiveMods.Remove(ActiveModSelected);
            ActiveModSelected = null;
            OnPropertyChanged(nameof(ActiveModSelected));
            OnPropertyChanged(nameof(ActiveModsCount));
            OnPropertyChanged(nameof(Hash));
            Status = "";
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (AllModSelected == null) return;
            if (ActiveMods.Any(i => i.Path == AllModSelected.Path)) return;
            ActiveMods.Add(AllModSelected);
            OnPropertyChanged(nameof(Hash));
            OnPropertyChanged(nameof(ActiveModsCount));
            Status = "";
        }

        private void BtnRefresh_Click(object sender, RoutedEventArgs e)
        {
            AllMods = new ObservableCollection<Cmod>(Cmod.GetAvailableMods());
            foreach (var mod in ActiveMods)
            {
                if (!Cmod.CheckMod(mod.Path))
                    mod.IsFound = false;
                mod.PathSimple = mod.Path.Replace("ugc_", "").Replace(".mod", "");
            }
            OnPropertyChanged(nameof(AllModsCount));
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Cmod.Apply(ActiveMods.ToList());
            Status = "Applied";
        }

        private void BtnRestore_Click(object sender, RoutedEventArgs e)
        {
            ActiveMods = new ObservableCollection<Cmod>(Cmod.GetActiveMods());
            OnPropertyChanged(nameof(Hash));
            foreach (var mod in ActiveMods)
            {
                if (!Cmod.CheckMod(mod.Path))
                    mod.IsFound = false;
                mod.PathSimple = mod.Path.Replace("ugc_", "").Replace(".mod", "");
            }
            OnPropertyChanged(nameof(ActiveModsCount));
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("steam://rungameid/281990");
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start("http://steamcommunity.com/sharedfiles/filedetails/?id=" +
                          (sender as Button)?.Tag);
            //new WndWebBrowser("http://steamcommunity.com/sharedfiles/filedetails/?id=" +(sender as Button).Tag).Show();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Rectangle_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void label3_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            new WndWebBrowser("http://store.steampowered.com/login").Show();
        }

        private void BtnSubAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (var mod in ActiveMods)
            {
                if (mod.IsFound) continue;
                var wnd = new WndWebBrowser(
                    "http://steamcommunity.com/sharedfiles/filedetails/?id=" +
                    mod.Path.Replace("ugc_", "").Replace(".mod", ""), true);
                wnd.Show();
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}