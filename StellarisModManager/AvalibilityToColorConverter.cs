﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace StellarisModManager
{
    public class AvalibilityToColorConverter : IValueConverter
    {
        private readonly Brush _none = new SolidColorBrush(Colors.Transparent);
        private readonly Brush _red = new SolidColorBrush(Colors.LightCoral);

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null && (bool) value ? _none : _red;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}