﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace StellarisModManager
{
    public class Cmod
    {
        public bool IsFound { get; set; } = true;
        public string Name { get; set; }
        public string Path { get; set; }
        public string PathSimple { get; set; } = "";

        public override string ToString()
        {
            return Name + "|" + Path;
        }

        private static int GetHash(IEnumerable<Cmod> list)
        {
            unchecked
            {
                return list.Aggregate(19,
                    (current, foo) => current * 31 + foo.Path.GetHashCode() + foo.Name.GetHashCode());
            }
        }

        public static string GetHashOfList(List<Cmod> list)
        {
            var str = GetHash(list.OrderBy(l => l.Path).ToList()).ToString();
            return str.Length >= 4 ? str.Substring(str.Length - 4) : "0000";
        }

        public static List<Cmod> GetAvailableMods()
        {
            var res = new List<Cmod>();
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                       "\\Paradox Interactive\\Stellaris\\mod";
            var listoffiles = Directory.GetFiles(path).ToList();
            listoffiles.ForEach(f =>
            {
                if (!f.EndsWith(".mod")) return;
                var s = File.ReadAllLines(f);
                res.Add(new Cmod
                {
                    Path = System.IO.Path.GetFileName(f),
                    Name = s.First(st => st.StartsWith("name=\"")).Replace("name=\"", "").Replace("\"", ""),
                    IsFound = true
                });
            });
            return res.OrderBy(r => r.Name, StringComparer.CurrentCulture).ToList();
        }

        public static void SaveCollection(string path, List<Cmod> collection)
        {
            File.WriteAllLines(path, collection.Select(c => c.ToString()).ToList());
        }

        public static List<Cmod> LoadCollection(string path)
        {
            return (from s in File.ReadAllLines(path)
                select new Cmod
                {
                    Name = s.Split('|')[0],
                    Path = s.Split('|')[1]
                }).ToList();
        }

        public static string GetName(string fileName)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                       "\\Paradox Interactive\\Stellaris\\mod\\";
            var t = fileName.Trim().Trim('"').Split('/')[1];
            var s = File.ReadAllLines(path + t);
            return s.First(st => st.StartsWith("name=\"")).Replace("name=\"", "").Replace("\"", "");
        }

        public static List<Cmod> GetActiveMods()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                       "\\Paradox Interactive\\Stellaris\\settings.txt";
            var ss = File.ReadAllLines(path);
            const string needle = "last_mods={";
            var flag = false;
            var res = new List<Cmod>();
            foreach (var t in ss)
            {
                if (flag)
                {
                    if (t == "}")
                        break;
                    res.Add(new Cmod
                    {
                        Name = GetName(t),
                        Path = t.Trim().Trim('"').Split('/')[1]
                    });
                }
                if (t == needle)
                    flag = true;
            }
            return res;
        }

        public static bool CheckMod(string modpath)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                       "\\Paradox Interactive\\Stellaris\\mod\\";
            return File.Exists(path + modpath);
        }

        public static void Apply(List<Cmod> collection)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                       "\\Paradox Interactive\\Stellaris\\settings.txt";
            var ss = File.ReadAllLines(path);
            var output = new List<string>();
            const string needle = "last_mods={";
            var flag = false;
            var added = false;
            foreach (var t in ss)
                if (t == needle)
                {
                    flag = true;
                    added = true;
                    output.Add(t);
                    output.AddRange(collection.Select(cmod => "\t\"mod/" + cmod.Path + "\""));
                    output.Add("}");
                }
                else
                {
                    if (!flag)
                    {
                        output.Add(t);
                    }
                    else
                    {
                        if (t == "}")
                            flag = false;
                    }
                }
            if (!added)
            {
                output.Add(needle);
                output.AddRange(collection.Select(cmod => "\t\"mod/" + cmod.Path + "\""));
                output.Add("}");
            }
            File.WriteAllLines(path, output);
        }
    }
}