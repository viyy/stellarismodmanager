﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Navigation;

namespace StellarisModManager
{
    /// <summary>
    ///     Логика взаимодействия для WndWebBrowser.xaml
    /// </summary>
    public partial class WndWebBrowser : Window
    {
        private readonly bool _sb;
        public bool LoadedFlag;

        public WndWebBrowser()
        {
            InitializeComponent();
            dynamic activeX = WBr.GetType().InvokeMember("ActiveXInstance",
                BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null, WBr, new object[] { });

            activeX.Silent = true;
        }

        public WndWebBrowser(string path, bool Subscribe = false)
        {
            InitializeComponent();
            WBr.Navigate(new Uri(path));
            dynamic activeX = WBr.GetType().InvokeMember("ActiveXInstance",
                BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null, WBr, new object[] { });

            activeX.Silent = true;
            _sb = Subscribe;
            WBr.LoadCompleted += _loadcompleted;
        }

        private void _loadcompleted(object sender, NavigationEventArgs e)
        {
            LoadedFlag = true;
            if (_sb) Subscribe();
        }

        public void Subscribe()
        {
            if (!LoadedFlag) return;
            try
            {
                WBr.InvokeScript("SubscribeItem");
            }
            catch (Exception)
            {
                const string msg = "Could not call script";
                MessageBox.Show(msg);
            }
            Close();
        }
    }
}